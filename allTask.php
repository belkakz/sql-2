<?php 
	require_once('functions.php');
	if (!isAuth()) {
		showError403();
	}
	//Получение списка всех твоих задач
 	 $pdo = create_pdo();
	 $sql = "SELECT id, user_id, assigned_user_id, description, date_added, is_done FROM task WHERE user_id LIKE ?";
	 $sth=$pdo->prepare($sql);
 	 $sth->execute([$_SESSION['user_id']]);
	 $myTask=$sth->fetchAll(PDO::FETCH_ASSOC);
	 

	//Получение списка всех делегированых тебе задач
 	 $pdo = create_pdo();
	 $sql = "SELECT t.id, t.assigned_user_id, t.description, t.date_added, t.is_done, u.login as author FROM task t JOIN user u ON u.id=t.user_id WHERE t.assigned_user_id LIKE ? AND t.user_id NOT LIKE ?";
	 $sth=$pdo->prepare($sql);
 	 $sth->execute([$_SESSION['user_id'], $_SESSION['user_id']]);
	 $assignTask=$sth->fetchAll(PDO::FETCH_ASSOC);

	 //Получение списка всех пользователей
 	 $pdo = create_pdo();
	 $sql = "SELECT id, login FROM user";
	 $sth=$pdo->prepare($sql);
	 $sth->execute();
	 $allUsers=$sth->fetchAll(PDO::FETCH_ASSOC);


	 //Делегирование задачи
	 if ((!empty($_POST)) && ($_POST['assigned_user_id'] !== $_SESSION['user_id'])) {
	 	$sql = "UPDATE task SET assigned_user_id={$_POST['assigned_user_id']} WHERE id={$_POST['task_id']} LIMIT 1";
 		$pdo->exec($sql);
		header('location: allTask.php');

	 }

	 function delegatePrint($allUsers, $myTask) {
	 		include('delegateprint.php');
	 }



	 if ((!empty($_GET)) && (isset($_GET['done']))) {
	 	$sql = "UPDATE task SET is_done={$_GET['done']} WHERE id={$_GET['id']} AND id={$_GET['id']} LIMIT 1";
	 	$pdo->exec($sql);
	 	header('location: allTask.php');
	 }  elseif ((!empty($_GET)) && (isset($_GET['delete']))) {
		 	$sql = "DELETE FROM task WHERE user_id={$_SESSION['user_id']} AND id={$_GET['id']} LIMIT 1";
		 	$pdo->exec($sql);
		 	header('location: allTask.php');
	 }
 ?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Все дела</title>
 	<style>
 		th{
 			background: #c4c4c4;
 		}
 		td, th {
 			border: 1px solid #dfe2e5;
			padding: 6px 13px;
			text-align: center;
 		}
 	</style>
 </head>
 <body>
<h3>Вы: <?=$_SESSION['username']?></h3>
<table>
	<thead>
		<tr>
		<th>Дела</th>
		<th>Когда</th>
		<th>Выполнено/Невыполнено</th>
		<th>Удалить</th>
		<th>Исполнитель</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			foreach ($myTask as $key => $value):
				$date = new DateTime($value['date_added']);
				$date_added = $date->format('d.m.Y');
				?>
				<tr>
				<td><?=$value['description']?></td>
				<td><?=$date_added?></td>
				<?php 
				if ($value['is_done']) {
					echo '<td><a href="./allTask.php?id='.$value['id'].'&done=0">Не выполнено</a></td>';
				}	else echo '<td><a href="./allTask.php?id='.$value['id'].'&done=1">Выполнить</a></td>';
				?>
				<td><a href="./allTask.php?id=<?=$value['id']?>&delete=1" style="color:red">Удалить</a></td>
				<td>
				<?php 
					delegatePrint($allUsers, $value);
				?>
				</td> </tr>
			<?php endforeach; ?>
	</tbody>
</table>
<?php if (!empty($assignTask)): ?>
	

<h2>Вам делегировано:</h2>
<table>
	<thead>
		<tr>
		<th>автор</th>
		<th>Дела</th>
		<th>Когда</th>
		<th>Выполнено/Невыполнено</th>
		<th>Удалить</th>
		<th>Исполнитель</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			foreach ($assignTask as $key => $value):
				$date = new DateTime($value['date_added']);
				$date_added = $date->format('d.m.Y');
				 $username = $value['author'];
				?>
				<tr>
					<td><?=$value['author']?></td>
					<td><?=$value['description']?></td>
					<td><?=$date_added?></td>
					<?php  if ($value['is_done']):?>
						<td><a href="./allTask.php?id=<?=$value['id']?>&done=0">Не выполнено</a></td>
					<?php else: ?><td><a href="./allTask.php?id=<?=$value["id"]?>&done=1">Выполнить</a></td><?php endif ?>
					<td><a href="./allTask.php?id=<?=$value['id']?>&delete=1" style="color:red">Удалить</a></td>
					<td>
					<?php  
						delegatePrint($allUsers, $value);
					?>
					</td> 
				</tr>
			<?php endforeach; ?> 
	</tbody>
</table>
<?php endif ?>
<a href="index.php">Главная</a><br>
<a href="add.php">Добавить еще дела</a><br>
<a href="index.php?out=1">Выйти</a>

 </body>
 </html> 