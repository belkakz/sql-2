<?php 
	require_once('functions.php');
	if (!empty($_POST)) {
		$pdo = create_pdo();
		$login = $_POST['login'];
		$pass = $_POST['password'];

		$sql = "SELECT id, login, password FROM user WHERE login LIKE ?";
		$sth=$pdo->prepare($sql);
		$sth->execute([$login]);
		$newBase=$sth->fetchAll(PDO::FETCH_ASSOC);

		if (!empty($newBase)) {
			$newBase = $newBase[0];
			if (($newBase['login'] === $login) && ($newBase['password'] === $pass)) {
				$_SESSION['username'] = $login;
				$_SESSION['user_id'] = $newBase['id'];
				if (isset($_SESSION['noLogin'])) {
					unset($_SESSION['noLogin']);
					unset($_SESSION['noPass']);
				}
				header('location: index.php');

			}	else {	
						if (isAuth($_SESSION['username'])) {
							echo "Вы уже вошли!";
						} else {
							noTrueLogin($login, $pass);
						}
					}
		
		}else noTrueLogin($login, $pass);
	} 
 ?>