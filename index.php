<?php 
require_once('functions.php');

if (isset($_SESSION['haveLogin'])) {
	echo "Такой пользователь уже существует, попробуйте еще раз!<br>";
}
if ((isset($_GET['login']) && ($_GET['login'] === 'singin'))) {
	singin();
}
if ((!empty($_SESSION)) && (!empty($_GET))) {
	if ((isset($_SESSION['noLogin'])) && (isset($_SESSION['noPass'])) && ($_GET['login']) === 'login') {
	login($_SESSION['noLogin'], $_SESSION['noPass']);
}	else {
		if ((isset($_GET['login']) && ($_GET['login'] === 'login'))) {
			login();
		}
	}
}
if ((isset($_GET['login']) && ($_GET['login'] === 'login')) && (!isset($_SESSION['noLogin']))) {
			login();
		}


if (isset($_GET['out'])) {
	session_destroy();
	header('location: index.php');
}
if (isAuth()) {
	echo "Привет {$_SESSION['username']}";
}	else echo '<a href="index.php?login=singin">Зарегистрироваться</a><br>
 	<a href="index.php?login=login">Войти</a>';
 ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>TODOlist</title>
 </head>
 <body>
 	<?php 
 		if (isAuth()) {
 			echo ' <br><a href="allTask.php">Все ваши дела</a>';
 			echo ' <br><a href="./add.php">Добавить дело</a>';
 			echo '<br><a href="index.php?out=1">Выйти</a>';
 	}

 	 ?>
 </body>
 </html>

